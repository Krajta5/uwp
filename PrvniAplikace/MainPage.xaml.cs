﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.Media.Capture;
using Windows.Storage;

namespace PrvniAplikace
{

    public sealed partial class MainPage : Page
    {
        Nastaveni nastaveni = new Nastaveni();

        public MainPage()
        {
            this.InitializeComponent();

            //zapnu preview
            OtevriKameru();
        }
            
        public async void OtevriKameru()
        {
            await cameraPreview.StartAsync();
        }
 
        //zavolat při zachycení fotky
        private async void Scan_Click(object sender, RoutedEventArgs e)
        {
            CameraCaptureUI captureUI = new CameraCaptureUI();

            //volba formátu
            captureUI.PhotoSettings.Format = nastaveni.Format;

            //velikost výsledné fotky z nastavení
            int sirka = nastaveni.Sirka;
            int vyska = nastaveni.Vyska;

            captureUI.PhotoSettings.CroppedSizeInPixels = new Size(sirka, vyska);

            String nazevFotky = DateTime.Now.ToFileTime().ToString();

            StorageFile photo = await captureUI.CaptureFileAsync(CameraCaptureUIMode.Photo);

            if (photo == null)
            {
                // User cancelled photo capture
                return;
            }

            //cílocá složka z nastavení
            StorageFolder umisteni = nastaveni.CilovaSlozka;

            //při kolizi vygeneruje nový název
            await photo.CopyAsync(umisteni, nazevFotky +"."+ captureUI.PhotoSettings.Format, NameCollisionOption.GenerateUniqueName);
            await photo.DeleteAsync();
        }
        

        private void NastaveniButton_Click(object sender, RoutedEventArgs e)
        {
            //přehodím frame
            Window.Current.Content = nastaveni;
            Window.Current.Activate();
        }

        private void CameraPreview_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Scan_Click(null, null);
        }
    }
}
