﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.UI.ViewManagement;
using Windows.Graphics.Display;
using Windows.Storage.Pickers;
using Windows.UI;

namespace PrvniAplikace
{

    public sealed partial class Nastaveni : Page
    {
        //kam bude ukládáno
        private String cesta = PickerLocationId.PicturesLibrary.ToString();
        private FolderPicker fp = new FolderPicker();

        private StorageFolder cilovaSlozka;
        private int vyska;
        private int sirka;
        //výchozí hodnota
        private CameraCaptureUIPhotoFormat format = CameraCaptureUIPhotoFormat.Jpeg;

        public StorageFolder CilovaSlozka
        {
            get { return cilovaSlozka; }
        }

        public int Sirka
        {
            get { return sirka; }
        } 

        public int Vyska
        {
            get { return vyska; }
        }

        public CameraCaptureUIPhotoFormat Format
        {
            get { return format; }
        }
        public Nastaveni()
        {
            this.InitializeComponent();
            
            //získám rozlišení ZOBRAZENÍ pro defaultní nastavení velikosti
            var bounds = ApplicationView.GetForCurrentView().VisibleBounds;
            var scaleFactor = DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            var size = new Size(bounds.Width * scaleFactor, bounds.Height * scaleFactor);

            //nastavím hodnoty rozlišení do textboxu
            textBoxSirka.Text = size.Width.ToString();
            textBoxVyska.Text = size.Height.ToString();

            //nastavím výchozí cestu pro uložení
            textBlockUloziste.Text = cesta;

            //výchozí složka pro uložení budou obrázky - plocha není v položkách KnownFolders
            fp.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            //musí být přidán filter jinak vyhozena vyjímka
            fp.FileTypeFilter.Add("*");

            //nutné povolit -> Package.appxmanifest -> Capabilities -> Pictures library
            cilovaSlozka = KnownFolders.PicturesLibrary;
        }

        private async void ButtonUloziste_Click(object sender, RoutedEventArgs e)
        {
            cilovaSlozka = await fp.PickSingleFolderAsync();

            if (cilovaSlozka != null)
            {
                textBlockUloziste.Text = cilovaSlozka.Path;
            }
        }
        
        //po kliknutí na uložit nastavení zapne preview
        private void UlozitNastaveniButton_Click(object sender, RoutedEventArgs e)
        {

          //kontrola zadaných rozměrů - zvýrazní špatně zadanou hodnotu uživateli
          SolidColorBrush cervenyStetec = new SolidColorBrush();
          cervenyStetec.Color = Colors.Red;
          SolidColorBrush cernyStetec = new SolidColorBrush();
          cernyStetec.Color = Colors.Black;

          //kontrola zadaných hodnot velikosti fotky, oznámení uživateli
          if (int.TryParse(textBoxSirka.Text, out sirka) == false)
          {
              textBoxSirka.Foreground = cervenyStetec;
              return;
          }
          //pokud uživatel chybu opravil
          else
          {
              textBoxSirka.Foreground = cernyStetec;
          }

          if (int.TryParse(textBoxVyska.Text, out vyska) == false)
          {
              textBoxVyska.Foreground = cervenyStetec;
              return;
          }
          //pokud uživatel chybu opravil
          else
          {
              textBoxVyska.Foreground = cernyStetec;
          }

            
          //otevřu frame s preview
          Frame frame = new Frame();
          Window.Current.Content = frame;
          frame.Navigate(typeof(MainPage));
          Window.Current.Activate();
        }

        private void FormatJPEG_Checked(object sender, RoutedEventArgs e)
        {
            format = CameraCaptureUIPhotoFormat.Jpeg;
        }

        private void FormatPNG_Checked(object sender, RoutedEventArgs e)
        {
            format = CameraCaptureUIPhotoFormat.Png;
        }
    }
}
